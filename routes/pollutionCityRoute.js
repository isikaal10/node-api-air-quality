const express = require('express');

const router = express.Router();

const mongoose = require('mongoose');
mongoose.set('debug', true);


const pollutionCityCtrl = require('../controllers/pollutionCityController');

// recuperation 1 enregistrement ( = 1 collection)
//router.get('/:id',stuffCtrl.getOneThing);

router.get('/city/:city',pollutionCityCtrl.getAirQualityByCityNow);
  
// recuperation de toutes les collections en date du jour
router.get('/citiestoday', pollutionCityCtrl.getAirQualityAllCitiesToday);

router.get('/counttoday/:city', pollutionCityCtrl.countAirQualityCityToday);

router.get('/cityhistory/:city', pollutionCityCtrl.getAirQualityByCityLastFifteenDays);

router.get('/citieshistory/:city', pollutionCityCtrl.getAirQualityByCitiesLastFifteenDays);

router.get('/regionstoday', pollutionCityCtrl.getAirQualityByRegionLastUpdate);

router.get('/citiesLastUpdate', pollutionCityCtrl.getAirQualityAllCitiesLastPollutionUpdate);

router.get('/countLastUpdateCities', pollutionCityCtrl.getAirQualityAllCitiesLastPollutionUpdate);




//router.get('/', pollutionCityCtrl.getAllPollutionCitiesToday);

// creation 1 enregistrement
//router.post('/', stuffCtrl.createThing);

// modification 1 enregistrement par son id
//router.put('/:id', stuffCtrl.modifyThing);

// supression d'un enregistrement par son id
//router.delete('/:id',stuffCtrl.deleteThing);

module.exports = router;