const PollutionCity = require ('../models/PollutionCityModel');
const PollutionRegion = require ('../models/PollutionRegionModel');
var moment = require('moment'); // require
moment().format();

const mongoose = require('mongoose');

const start = new Date();
start.setUTCHours(0,0,0,0);


/*exports.getAllPollutionCities = (req, res, next) => {
  PollutionCity.find()
  .then(pollutionCities => res.status(200).json(pollutionCities))
  .catch(error => res.status(400).json({ error }));
};*/

/* *************  POUR TOUTES LES VILLES DEBUT  **************   */
// find tous les documents en date du jour pour toutes les villes
exports.getAirQualityAllCitiesToday = (req, res, next) => {
  //Thing.findOne({ _id: req.params.id })
  
 PollutionCity.find({  day: { $gt: start , $lt: Date.now()} }).sort({day: -1})
  .then(data =>res.status(200).json(data)
  )
  .catch(error => res.status(400).json({ error }));
};

// find les données pour toutes les villes sur les 15 derniers jours
exports.getAirQualityByCitiesLastFifteenDays = (req, res, next) => {
  PollutionCity.find( {  day: { $gt: Date.now() - (1000 * 60 * 60 * 360) , $lt: Date.now()}})
  .then(pollutioncities => res.status(200).json(pollutioncities))
  .catch(error => res.status(400).json({ error }));
  //PollutionCity.find({  day: { $gt: Date.now() - (1000 * 60 * 60 * 6) , $lt: Date.now() } , cityName: req.params.city } ) 
  //PollutionCity.find({  day: {$eq:  new Date().toISOString()}, cityName: req.params.city } )
};
/* *************  POUR TOUTES LES VILLES FIN  **************   */

/* ****************** DONNEES REGIONS DEBUT ********************* */
exports.getAirQualityByRegionLastUpdate= (req, res, next) => {

    PollutionRegion.aggregate([
    {
        $group:
        {
            _id: { regionNumber: "$regionNumber" },
            totalPollution: { $sum: "$pollutionIndexLastUpdate" },
        }
    }
  ])
  .then(data => res.status(200).json(data))
  .catch(error => res.status(400).json({ error }));
};

/* ****************** DONNEES REGIONS FIN ********************* */

/* *************  PAR VILLE DEBUT  **************   */
exports.countAirQualityCityToday = (req, res, next) => {
  //Thing.findOne({ _id: req.params.id })
  PollutionCity.countDocuments({  day: { $gt: start , $lt: Date.now()} , cityName: req.params.city}).distinct('day')
  .then(pollutionCity => res.status(200).json(pollutionCity))
  .catch(error => res.status(400).json({ error }));
};

// find les données à date du jour pour une ville donnée
//cityName: req.params.city
exports.getAirQualityByCityNow = (req, res, next) => {
  PollutionCity.find( {  day: { $gt: start , $lt: Date.now() } , cityName: req.params.city })
  .then(pollutioncities => res.status(200).json(pollutioncities))
  .catch(error => res.status(400).json({ error }));
  //PollutionCity.find({  day: { $gt: Date.now() - (1000 * 60 * 60 * 6) , $lt: Date.now() } , cityName: req.params.city } ) 
  //PollutionCity.find({  day: {$eq:  new Date().toISOString()}, cityName: req.params.city } )
};

// find les données pour une ville sur les 15 derniers jours
exports.getAirQualityByCityLastFifteenDays = (req, res, next) => {
  PollutionCity.find( {  day: { $gt: Date.now() - (1000 * 60 * 60 * 360) , $lt: Date.now()} , cityName: req.params.city })
  .then(pollutioncities => res.status(200).json(pollutioncities))
  .catch(error => res.status(400).json({ error }));
  //PollutionCity.find({  day: { $gt: Date.now() - (1000 * 60 * 60 * 6) , $lt: Date.now() } , cityName: req.params.city } ) 
  //PollutionCity.find({  day: {$eq:  new Date().toISOString()}, cityName: req.params.city } )
};

exports.getAirQualityAllCitiesLastPollutionUpdate = (req, res, next) => {
 PollutionRegion.find() 
 .then(data => res.status(200).json(data))
  .catch(error => res.status(400).json({ error }));
};

exports.countAirQualityAllCitiesLastPollutionUpdate = (req, res, next) => {
  PollutionRegion.count() 
  .then(data => res.status(200).json(data))
   .catch(error => res.status(400).json({ error }));
 };
/* *************  PAR VILLE FIN  **************   */




/*exports.createThing = (req, res, next) => {
    delete req.body._id;
    const thing = new PollutionCity({
      ...req.body
    });
    thing.save()
    // IMPORTANT! => dans le then() on renvoit une reponse au front end sinon expiration de la requete 
    .then(() => res.status(201).json({ message: 'Objet enregistré !'}))
    .catch(error => res.status(400).json({ error }));
};*/

/*exports.modifyThing = (req, res, next) => {
    Thing.updateOne({ _id: req.params.id }, { ...req.body, _id: req.params.id })
      .then(() => res.status(200).json({ message: 'Objet modifié !'}))
      .catch(error => res.status(400).json({ error }));
};

exports.deleteThing = (req, res, next) => {
    Thing.deleteOne({ _id: req.params.id })
      .then(() => res.status(200).json({ message: 'Objet supprimé !'}))
      .catch(error => res.status(400).json({ error }));
};

exports.getOneThing =  (req, res, next) => {
    Thing.findOne({ _id: req.params.id })
      .then(thing => res.status(200).json(thing))
      .catch(error => res.status(404).json({ error }));
};
*/

// recuperation plusieurs enregistrements ( = plusieurs collections)
  /*app.get('/pollutioncities', (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');

    PollutionCity.find()
    //.then(pollutioncities => res.status(200).json(pollutioncities))
    .then(pollutioncities => res)
    .catch(error => res.status(400).json({ error }));
  });*/
