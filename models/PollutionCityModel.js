const mongoose = require ('mongoose');

const PollutionCitiesSchema = mongoose.Schema ({
    
    cityName: {type: String, require: true},
    pollutionIndex: {type: Number},
    day:   {type: Date},
    cityGeoLatitude: {type: Number},
    cityGeoLongitude: {type: Number},
    regionNumber:{type:Number, require:true},
    regionName:{type:String},
    co:{type:Number},
    so2:{type:Number},
    no2:{type:String},
    o3:{type:Number},
    t:{type:Number},
    h:{type:Number},
    w:{type:Number},
    p:{type:Number}
});

module.exports = mongoose.model('PollutionCity', PollutionCitiesSchema);




