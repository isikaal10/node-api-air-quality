const mongoose = require ('mongoose');

const PollutionRegionsSchema = mongoose.Schema ({
    
        regionName: {type: String, require: true},
        regionNumber:{type:Number, require:true},
        day:   {type: Date},
        cityGeoLatitude: {type: Number},
        cityGeoLongitude: {type: Number},
        cityName:{type: String},    
        pollutionIndexLastUpdate :{type:Number} // correspond à aqi
});
//co: monoxyde de carbone /  no2 dioxyde d' azote /  o3 ozone / t temperature / h humidity / w vent  / p pression atmospherique
 
module.exports = mongoose.model('PollutionRegion', PollutionRegionsSchema);





